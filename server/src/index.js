const express = require('express');
const cors = require('cors');
const morgan = require('morgan')

// App
const app = express();
const port = 3030;

app.set('port', port);

app.use(cors());
app.use(morgan('dev'));
app.use(express.urlencoded({
    extended: true, 
    limit: '50mb'
}));
app.use(express.json({limit: '50mb'}));

app.get('/', (req, res) => {
  res.send('mi CI esta funcionando prueba3');
});
app.get('/test/servicio1', (req, res) => {
  res.send('Mi primer servicio con ansible: ' + MENSAJE);
});


app.listen(app.get('port'), () => {
    console.log('Api Corriendo en http://localhost:' + port); 
});